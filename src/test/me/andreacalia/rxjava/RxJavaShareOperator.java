package me.andreacalia.rxjava;

import io.reactivex.Flowable;
import io.reactivex.Scheduler;
import io.reactivex.processors.PublishProcessor;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subscribers.TestSubscriber;
import org.assertj.core.api.Assertions;
import org.junit.Test;

import java.time.Instant;
import java.util.concurrent.TimeUnit;

import static org.assertj.core.api.Assertions.assertThat;


public class RxJavaShareOperator {

    @Test
    public void testSlowClient() {
        PublishProcessor<String> source = PublishProcessor.create();
        Flowable<String> shared = source.observeOn(Schedulers.newThread()).share();

        TestSubscriber<String> slowSubscriber = shared.doOnNext(v -> await(10_000)).test();
        TestSubscriber<String> fastSubscriber = shared.doOnNext(v -> await(10)).test();

        source.onNext("A");
        source.onNext("B");

        await(1_000);

        /*
        Fast subscriber does not get any value since the propagation of the share is blocked in the slow subscriber.
        Possible race condition if the implementation decides to deliver first to the fastSubscriber
         */
        assertThat(fastSubscriber.values().size()).isLessThanOrEqualTo(1);
        assertThat(slowSubscriber.values()).isEmpty();
    }

    @Test
    public void testSlowClientWithObserveOnNoBuffer() {
        PublishProcessor<String> source = PublishProcessor.create();
        Flowable<String> shared = source.observeOn(Schedulers.newThread()).share();

        TestSubscriber<String> slowSubscriber = shared
                .observeOn(Schedulers.newThread(), false, 1)
                .doOnNext(v -> await(10_000)).test();

        TestSubscriber<String> fastSubscriber = shared
                .observeOn(Schedulers.newThread(), false, 1)
                .doOnNext(v -> await(10)).test();

        source.onNext("A");
        source.onNext("B");

        await(1_000);

        /*
        The observeOn switches the context, so the fast subscriber is not blocked.
        The problem is that the share waits for all the subscribers to request before delivering values.
        This still prevents the fast subscriber to get the second value
         */
        assertThat(fastSubscriber.values()).containsOnly("A");
        assertThat(slowSubscriber.values()).isEmpty();
    }

    @Test
    public void testSlowClientWithObserveOnNoBufferButRequestsEverythingFromUpstream() {
        PublishProcessor<String> source = PublishProcessor.create();
        Flowable<String> shared = source.observeOn(Schedulers.newThread()).share();

        TestSubscriber<String> slowSubscriber = shared
                .onBackpressureLatest()
                .observeOn(Schedulers.newThread(), false, 1)
                .doOnNext(v -> await(10_000)).test();

        TestSubscriber<String> fastSubscriber = shared
                .onBackpressureLatest()
                .observeOn(Schedulers.newThread(), false, 1)
                .doOnNext(v -> await(10)).test();

        source.onNext("A");
        source.onNext("B");

        await(1_000);

        /*
        The onBackpressureLatest implicitly do a request(Long.MAX_VALUE) upstream so the share will push values as soon as they arrive.
         */
        assertThat(fastSubscriber.values()).containsOnly("A", "B");
        assertThat(slowSubscriber.values()).isEmpty();
    }

    private static void await(int timeout) {
        try {
            TimeUnit.MILLISECONDS.sleep(timeout);
        } catch (InterruptedException e) {
            /* */
        }
    }

}
